$(document).ready(function () {
    // init carousel
    $(".carousel-fix").carousel({
        interval: 5000
    });

    $(".hamburger").on("click", function () {
        $(".dropdown-nav").slideToggle(200);
    });

    $(".hamburger-container .nav-li").on("click", function () {
        $(".dropdown-nav").slideToggle(100);
    });

    $(".sasomange-img").on("click", function () {
        const win = window.open("https://www.facebook.com/saso.mange.system/", "_blank");

        if (win) {
            win.focus();
        } else {
            alert("Please allow popups for this page!");
        }
    });
});